# Envision - Fullstack Web Developer Assignment

Assignment for the full stack developer role at Envision.

## Project: Envision Document Reader

Reading Documents is a big part of the Envision app. We'd like to you to reproduce a small portion of the Envision Document Reader on the web given the relevant REST API endpoints and design. The main aim for us here is to understand how you strucutre your code, if you're able to work with REST APIs well as well as your sense of design.

## Design

The design for this can be found in the Figma file: https://www.figma.com/file/mc8Ahvua2SU1sSpFLdchOp/%F0%9F%94%B5-Assignments?node-id=1%3A1397

The final output should be responsive and work well on devices of all screen sizes. The design should also be consistent across the most popular browsers. 

## Implementation

The user uploads an image from the device and using the API endpoint provided the text from the image is extracted. The extracted text is displayed in a new screen as shown in the design. 

### API Endpoint details

Endpoint: https://letsenvision.app/api/test/readDocument

Type: POST

Sample CURL usage: `curl --location --request POST 'https://letsenvision.app/api/test/readDocument' \  --form 'photo=@<filename>'`

Postman link(if you use Postman for testing API endpoints): https://www.postman.com/collections/771c175ea7a0e2db34b9

## Submission

- Please host your solution using any free hosting solution like Netlify or Heroku. It's purely for testing purposes. 

- Please fork this repository and submit your code and not submit PRs directly. 
